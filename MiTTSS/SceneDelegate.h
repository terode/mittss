//
//  SceneDelegate.h
//  MiTTSS
//
//  Created by Alex Sorokolita on 17.01.2020.
//  Copyright © 2020 Alex Sorokolita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

